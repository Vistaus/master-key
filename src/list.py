# list.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject, Gio, Adw
from master_key import Timer, Database, Row


class List(Gtk.ListBox):
    __gtype_name__ = 'List'

    __gsignals__ = {
        'items-changed': (GObject.SIGNAL_RUN_FIRST, None, ())
    }

    filter_text = GObject.Property(type=str)

    def __init__(self):
        super().__init__()
        self.connect('row-activated', self.on_row_activated)
        self.connect('notify::filter-text', self.on_filter_text_changed)
        self.active_row = None
        self._loaded = False
        self._model = Gio.ListStore()
        self._filter = Gtk.CustomFilter()
        self._filter.set_filter_func(self.filter_function)
        self._filteredModel = Gtk.FilterListModel(
            model=self._model
        )
        self._filteredModel.set_filter(self._filter)
        self.bind_model(self._filteredModel, lambda x: Row(x))
        self._model.connect('items-changed', lambda *args: self.emit('items-changed'))
        self._filteredModel.connect('items-changed', lambda *args: self.emit('items-changed'))

    def filter_function(self, password):
        text = self.props.filter_text.strip().lower()
        if text == '':
            return True
        if text in password.props.domain or text in password.props.username:
            return True
        return False

    def load(self):
        if not self._loaded:
            self.append(*Database.get_default().passwords)
            self._loaded = True
        self.emit('items-changed')

    def on_filter_text_changed(self, *args):
        self._filter.changed(Gtk.FilterChange.DIFFERENT)

    def collapse_all(self):
        if self.active_row:
            self.active_row.props.expanded = False
            self.active_row = None

    def clear(self):
        self.active_row = None

    @Timer.report_activity
    def on_row_activated(self, list_box, row):
        if self.active_row and self.active_row != row:
            self.active_row.props.expanded = False
        self.active_row = row

    def get_n_items(self):
        return len(self._model)

    def get_n_visible_items(self):
        return len(self._filteredModel)

    def remove(self, password):
        position = self._model.find(password).position
        self._model.remove(position)
        return position

    def insert(self, password, index=-1):
        if index < 0:
            self._model.append(password)
        else:
            self._model.insert(index, password)

    def append(self, *passwords):
        self._model.splice(
            self._model.get_n_items(),
            0,
            list(passwords)
        )

    def remove_all(self):
        self._loaded = True
        self._model.splice(
            0,
            self.get_n_items(),
            []
        )

