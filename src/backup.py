# backup.py
#
# Copyright 2021 Guillermo Peña
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
from master_key import Database, Password
import json

# TODO encrypt backup file
VERSION = 1


def export_passwords(filename: str):
    passwords = Database().get_default().passwords
    data = {
        'version': VERSION,
        'passwords': {
            i: item.to_json() for i, item in enumerate(passwords, 1)
        }
    }
    data = json.dumps(data, indent=4, ensure_ascii=False)
    with open(filename, 'w') as f:
        f.write(data)


def import_passwords(filename: str) -> ['Password']:
    with open(filename, 'r') as f:
        data = json.load(f)

    if not all(key in data for key in ['version', 'passwords']):
        raise

    passwords = []
    for _, password in data['passwords'].items():
        passwords.append(Password.create(*password.values()))
    return passwords

