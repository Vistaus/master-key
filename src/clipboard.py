# clipboard.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gdk, GLib
from master_key import Settings


class Clipboard:

    instance: 'Clipboard' = None
    timeout_id: int = 0

    def __init__(self):
        self.clipboard = Gdk.Display.get_default().get_clipboard()

    @staticmethod
    def get_default() -> 'Clipboard':
        if Clipboard.instance is None:
            Clipboard.instance = Clipboard()
        return Clipboard.instance

    def clear(self):
        self._remove_source()
        self.clipboard.clear()

    def copy(self, text: str, clear: bool = True):
        self.clipboard.set(text)
        self._remove_source()

        def timeout(*_):
            self.timeout_id = 0
            self.clipboard.set('')
            return False

        seconds = Settings.get_default().clipboard_timeout

        if clear:
            self.timeout_id = GLib.timeout_add_seconds(seconds, timeout)

    def _remove_source(self):
        if self.timeout_id == 0:
            return
        GLib.Source.remove(self.timeout_id)
        self.timeout_id = 0

