# password.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from master_key import logger
from gi.repository import GLib, GObject
from threading import Thread
from time import perf_counter
from functools import wraps
from gettext import gettext as _
from random import seed, choice, shuffle
from hashlib import pbkdf2_hmac
import string



def benchmark(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = perf_counter()
        result = func(*args, **kwargs)
        end = perf_counter()
        logger.debug(f'time taken {end-start} s')
        return result
    return wrapper


class Password(GObject.GObject):

    def __init__(self, *args):
        super().__init__()
        self.id_ = args[0]
        self.domain = args[1]
        self.username = args[2]
        self.lowercase = args[3]
        self.uppercase = args[4]
        self.digits = args[5]
        self.symbols = args[6]
        self.length = int(args[7])
        self.version = int(args[8])

    @staticmethod
    def create(*args) -> 'Password':
        from master_key import Database
        return Database.get_default().insert_password(*args)

    def update(self):
        from master_key import Database
        data = {
            'domain': self.domain,
            'username': self.username,
            'lowercase': self.lowercase,
            'uppercase': self.uppercase,
            'digits': self.digits,
            'symbols': self.symbols,
            'length': self.length,
            'version': self.version
        }
        Database.get_default().update_password(self.id_, **data)

    def delete(self):
        from master_key import Database
        Database.get_default().delete_password(self.id_)

    def generate_password(self, callback):
        @benchmark
        def target(*args, **kwargs):
            characters = {}
            if self.lowercase:
                characters['a'] = string.ascii_lowercase
            if self.uppercase:
                characters['A'] = string.ascii_uppercase
            if self.digits:
                characters['d'] = string.digits
            if self.symbols:
                characters['s'] = string.punctuation

            default = ''.join(characters.values())

            self._generate_seed()

            template = list(characters)
            template += ['x' for i in range(len(template), self.length)]
            shuffle(template)

            password = ''.join(
                choice(characters.get(i, default)) for i in template
            )

            GLib.idle_add(callback, password)

        t = Thread(target=target, daemon=True)
        t.start()

    def _generate_seed(self):
        from master_key import Database
        password = Database.get_default().passphrase
        salt = f'{self._domain}\0{self._username}\0{self._version * 1_000}'
        dk = pbkdf2_hmac(
            'sha512',
            password.encode('utf-8'),
            salt.encode('utf-8'),
            200_000,
            32
        )
        seed(dk, version=2)

    def to_json(self) -> {}:
        return {
            'domain': self.domain,
            'username': self.username,
            'lowercase': self.lowercase,
            'uppercase': self.uppercase,
            'digits': self.digits,
            'symbols': self.symbols,
            'length': self.length,
            'version': self.version
        }

    @GObject.Property(type=str)
    def domain(self):
        return self._domain

    @domain.setter
    def domain(self, text):
        self._domain = text

    @GObject.Property(type=str)
    def username(self):
        return self._username

    @username.setter
    def username(self, text):
        self._username = text

    @GObject.Property(type=str)
    def version_text(self):
        return self._version_text

    @version_text.setter
    def version_text(self, text):
        self._version_text = text

    @property
    def version(self):
        return self._version

    @version.setter
    def version(self, version):
        self._version = version
        self.version_text = '{}: {}'.format(_("version"), version)


__all__ = ['Password', 'benchmark']
