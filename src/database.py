# database.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import GLib, GObject
from os import path, makedirs, remove, rename
from master_key import logger, Password
from threading import Thread
import sqlite3
import json
import glob


sqlite3.register_adapter(bool, int)
sqlite3.register_converter('boolean', lambda v: bool(v))


def get_cipher_version() -> int:
    temp = sqlite3.connect(':memory:')
    cursor = temp.cursor()
    cursor.execute('PRAGMA cipher_version')
    version = cursor.fetchone()
    if not version:
        raise
    temp.close()
    return int(version[0][0])


class Database(GObject.GObject):

    __gtype_name__ = 'Database'
    __gsignals__ = {
        'connected': (GObject.SIGNAL_RUN_FIRST, None, (bool,))
    }

    VERSION = 1
    compat = False
    instance: 'Database' = None
    connection: sqlite3.Connection

    def __init__(self):
        super().__init__()
        self.db_dir = path.join(
            GLib.get_user_data_dir(),
            'master-key'
        )
        makedirs(self.db_dir, exist_ok=True)
        self.cipher_version = get_cipher_version()
        self.filename = self.get_db_filename()

    def get_db_filename(self) -> str:
        files = glob.glob(path.join(self.db_dir, 'database*.db'))

        if not files:
            self.props.exists = False
            return path.join(
                self.db_dir,
                f'database{self.cipher_version}-{Database.VERSION}.db'
            )

        cipher, version = filter(str.isnumeric, path.basename(files[0]))
        if int(cipher) < self.cipher_version:
            self.compat = True

        self.props.exists = True
        return files[0]

    @staticmethod
    def get_default() -> 'Database':
        if Database.instance is None:
            Database.instance = Database()
        return Database.instance

    @GObject.Property(type=bool, default=False)
    def exists(self):
        return self._exists

    @exists.setter
    def exists(self, exists):
        self._exists = exists

    def unlock(self, passphrase: str):

        def check_passphrase():
            connection = sqlite3.connect(
                self.filename, check_same_thread=False)
            connection.execute(f'PRAGMA key="{passphrase}"')
            if self.compat:
                connection.execute('PRAGMA cipher_compatibility=3')
            try:
                connection.execute('SELECT COUNT(*) FROM sqlite_master')
            except sqlite3.DatabaseError:
                GLib.idle_add(self.emit, 'connected', False)
                return

            if self.compat:
                connection.close()

                old_filename = self.filename
                self.filename = path.join(
                    self.db_dir,
                    f'database{self.cipher_version}-{Database.VERSION}.db'
                )

                rename(old_filename, self.filename)

                connection = sqlite3.connect(
                    self.filename, check_same_thread=False)
                connection.execute(f'PRAGMA key="{passphrase}"')
                connection.execute('PRAGMA cipher_migrate')

            self.passphrase = passphrase
            self.connection = connection
            self._create_tables()

            self.props.exists = True
            GLib.idle_add(self.emit, 'connected', True)

        t = Thread(target=check_passphrase, daemon=True)
        t.start()

    @property
    def passphrase(self) -> str:
        return self._passphrase

    @passphrase.setter
    def passphrase(self, passphrase):
        self._passphrase = passphrase

    def close(self):
        self.connection.close()
        self.passphrase = ''

    @property
    def passwords(self) -> [Password]:
        query = 'SELECT * FROM passwords'
        try:
            data = self.connection.cursor().execute(query)
            rows = data.fetchall()
            return [Password(*password) for password in rows]
        except sqlite3.OperationalError as error:
            logger.error(str(error))
        return []

    def insert_password(self, *args) -> Password:
        query = 'INSERT INTO passwords VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?)'
        cursor = self.connection.cursor()
        try:
            cursor.execute(query, args)
            self.connection.commit()
            return Password(cursor.lastrowid, *args)
        except sqlite3.OperationalError as error:
            logger.error(str(error))

    def delete_password(self, password_id: int):
        query = "DELETE FROM passwords WHERE id=?"
        try:
            self.connection.execute(query, [password_id])
            self.connection.commit()
        except sqlite3.Error as error:
            logger.error(str(error))

    def update_password(self, password_id: int, **kwargs):
        query = 'UPDATE passwords SET '
        query += ', '.join([f'{column}=?' for column in kwargs.keys()])
        query += ' WHERE id=?'
        try:
            self.connection.execute(
                query,
                list(kwargs.values()) + [password_id]
            )
            self.connection.commit()
        except sqlite3.OperationalError as error:
            logger.error(str(error))

    def _create_tables(self):
        cursor = self.connection.cursor()
        cursor.executescript(
            '''
                CREATE TABLE IF NOT EXISTS passwords (
                    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                    domain TEXT NOT NULL,
                    username TEXT NOT NULL,
                    lowercase INTEGER NOT NULL,
                    uppercase INTEGER NOT NULL,
                    digits INTEGER NOT NULL,
                    symbols INTEGER NOT NULL,
                    length INTEGER NOT NULL,
                    version INTEGER NOT NULL
                )
            '''
        )

    def reset(self):
        if self.props.exists:
            remove(self.filename)
            self.props.exists = False

