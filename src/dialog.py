# dialog.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk
from master_key import Timer
from gettext import gettext as _
import time


@Gtk.Template(resource_path='/com/gitlab/guillermop/MasterKey/ui/dialog.ui')
class Dialog(Gtk.Dialog):
    __gtype_name__ = 'Dialog'

    domain_entry = Gtk.Template.Child()
    username_entry = Gtk.Template.Child()
    version_entry = Gtk.Template.Child()
    length_entry = Gtk.Template.Child()
    lowercase_switch = Gtk.Template.Child()
    uppercase_switch = Gtk.Template.Child()
    digits_switch = Gtk.Template.Child()
    symbols_switch = Gtk.Template.Child()

    def __init__(self, parent, password=None, **kwargs):
        super().__init__(transient_for=parent, use_header_bar=True, **kwargs)
        Timer.get_default().connect('timeout', lambda _: self.destroy())
        self.password = password
        self.set_title(_('Edit password') if password else _('Add password'))

        if self.password:
            self._init_widgets()
            self.connect('response', self.on_response)
        self.domain_entry.grab_focus()

    def on_response(self, dialog, response):
        if response == Gtk.ResponseType.OK:
            self.password.domain = self.domain_entry.get_text()
            self.password.username = self.username_entry.get_text()
            self.password.version = int(self.version_entry.get_value())
            self.password.length = int(self.length_entry.get_value())
            self.password.lowercase = self.lowercase_switch.get_active()
            self.password.uppercase = self.uppercase_switch.get_active()
            self.password.digits = self.digits_switch.get_active()
            self.password.symbols = self.symbols_switch.get_active()
            self.password.update()

    def _init_widgets(self):
        password = self.password
        self.domain_entry.set_text(password.domain)
        self.username_entry.set_text(password.username)
        self.version_entry.set_value(password.version)
        self.length_entry.set_value(password.length)
        self.lowercase_switch.set_active(password.lowercase)
        self.uppercase_switch.set_active(password.uppercase)
        self.digits_switch.set_active(password.digits)
        self.symbols_switch.set_active(password.symbols)

    def get_data(self):
        return [
            self.domain_entry.get_text(),
            self.username_entry.get_text(),
            self.lowercase_switch.get_active(),
            self.uppercase_switch.get_active(),
            self.digits_switch.get_active(),
            self.symbols_switch.get_active(),
            int(self.length_entry.get_value()),
            int(self.version_entry.get_value())
        ]

    def validate(self):
        switches = [
            self.lowercase_switch.get_active(),
            self.uppercase_switch.get_active(),
            self.digits_switch.get_active(),
            self.symbols_switch.get_active(),
        ]
        return self.domain_entry.get_text() and self.username_entry.get_text() and any(switches)

    @Gtk.Template.Callback()
    @Timer.report_activity
    def update_response_buttons(self, *args):
        self.set_response_sensitive(Gtk.ResponseType.OK, self.validate())



