# __init__.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from .settings import Settings
from .password import Password
from .database import Database
from .timer import Timer
from .clipboard import Clipboard
from .label import Label
from .row import Row
from .dialog import Dialog
from .preferences import Preferences
from .list import List
from .window import Window, WindowState


class RestoreError(Exception):
    pass


