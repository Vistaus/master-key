# row.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject, Gio, Adw
from master_key import Timer, Clipboard
from gettext import gettext as _


@Gtk.Template(resource_path='/com/gitlab/guillermop/MasterKey/ui/row.ui')
class Row(Adw.ExpanderRow):
    __gtype_name__ = 'Row'

    password_label = Gtk.Template.Child()
    stack = Gtk.Template.Child()
    bump_button = Gtk.Template.Child()
    version_label = Gtk.Template.Child()

    def __init__(self, password):
        super().__init__()
        self.password = password
        self.bind_properties()
        self.action_group = Gio.SimpleActionGroup()
        self.insert_action_group('row', self.action_group)
        self.add_action('copy', self.copy)
        self.add_action('delete', self.delete)
        self.add_action('collapse', self.collapse)

    def add_action(self, key, callback):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        self.action_group.add_action(action)
        self.bind_property(
            "expanded",
            action,
            "enabled",
            GObject.BindingFlags.DEFAULT | GObject.BindingFlags.SYNC_CREATE
        )

    @Timer.report_activity
    def copy(self, *args):
        password = self.password_label.get_text()
        Clipboard.get_default().copy(password)
        self.get_root().overlay.add_toast(Adw.Toast(
            title=_("Password copied")
        ))

    @Timer.report_activity
    def delete(self, *args):
        self.get_root().delete_password(self.password)

    @Timer.report_activity
    def collapse(self, *args):
        self.props.expanded = False

    def bind_properties(self):
        self.password.bind_property(
            'domain', self, 'title', GObject.BindingFlags.SYNC_CREATE)
        self.password.bind_property(
            'username', self, 'subtitle', GObject.BindingFlags.SYNC_CREATE)
        self.password.bind_property(
            'version-text', self.version_label, 'label', GObject.BindingFlags.SYNC_CREATE)

    @Gtk.Template.Callback()
    def on_expanded_changed(self, *args):
        if self.props.expanded:
            if not self.password_label.get_text():
                self.generate_password()
        else:
            self.password_label.visibility = False
            self.stack.set_visible_child_name('label')

    @Gtk.Template.Callback()
    @Timer.report_activity
    def on_edit_button_clicked(self, *_):
        from master_key import Dialog
        window = self.get_root()
        dialog = Dialog(window, self.password)

        def on_response(d, response):
            d.destroy()
            self.generate_password()

        dialog.connect('response', on_response)
        dialog.present()

    def on_generated(self, password):
        self.password_label.set_text(password)
        self.stack.set_visible_child_name('label')
        self.bump_button.set_sensitive(True)

    def generate_password(self, *args):
        self.stack.set_visible_child_name('spinner')
        self.password.generate_password(self.on_generated)

    @Gtk.Template.Callback()
    @Timer.report_activity
    def on_bump_button_clicked(self, button):
        button.set_sensitive(False)
        self.grab_focus()
        self.password.version += 1
        self.password.update()
        self.generate_password()

