# application.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, Gdk, GObject, GLib, Adw
from master_key import Settings, Window, WindowState, Preferences, Timer, Database
from gettext import gettext as _


class Application(Adw.Application):

    is_locked = GObject.Property(type=bool, default=True)

    def __init__(self, version, application_id):
        super().__init__(
            application_id=application_id,
            flags=Gio.ApplicationFlags.FLAGS_NONE,
            resource_base_path="/com/gitlab/guillermop/MasterKey",
        )
        self.version = version
        settings = Settings(application_id)
        settings.connect("changed::dark-theme", self.on_dark_theme_changed)

    def do_startup(self):
        Adw.Application.do_startup(self)
        GLib.set_application_name(_('Master Key'))
        GLib.set_prgname(self.get_application_id())
        self.setup_actions()
        self.on_dark_theme_changed()
        self.connect("notify::is-locked", self.is_locked_changed)
        Timer.get_default().connect_after('timeout', self.lock)

    def setup_actions(self):
        self.add_action("quit", self.on_quit)
        self.add_action("about", self.on_about)
        reset = self.add_action("reset", self.on_reset)
        Database.get_default().bind_property(
            'exists',
            reset,
            'enabled',
            GObject.BindingFlags.DEFAULT | GObject.BindingFlags.SYNC_CREATE
        )
        lock = self.add_action("lock", self.lock)
        self.bind_locked(lock, 'enabled')
        self.add_action("preferences", self.on_preferences)
        self.set_accels_for_action("app.quit", ["<Ctrl>Q"])
        self.set_accels_for_action("app.preferences", ["<Ctrl>comma"])
        self.set_accels_for_action("app.lock", ["<Ctrl>L"])
        self.set_accels_for_action("win.add", ["<Ctrl>N"])
        self.set_accels_for_action("win.search", ["<Ctrl>F"])

    def add_action(self, key, callback):
        action = Gio.SimpleAction.new(key, None)
        action.connect("activate", callback)
        super().add_action(action)
        return action

    def bind_locked(self, target, target_property):
        self.bind_property(
            'is_locked',
            target,
            target_property,
            GObject.BindingFlags.INVERT_BOOLEAN | GObject.BindingFlags.SYNC_CREATE
        )

    def on_preferences(self, *_):
        preferences = Preferences()
        preferences.set_transient_for(self.props.active_window)
        preferences.present()

    def lock(self, source, *_):
        Timer.get_default().stop()
        self.is_locked = True

    def is_locked_changed(self, *_):
        if self.is_locked:
            win = self.props.active_window
            win.props.state = WindowState.LOGIN if Database.get_default().exists else WindowState.SETUP
        else:
            Timer.get_default().reset()

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = Window(application=self)
            if self.props.application_id.endswith('Devel'):
                win.get_style_context().add_class('devel')
            self.props.is_locked = True
        win.present()

    def on_dark_theme_changed(self, *_):
        color = Adw.ColorScheme.PREFER_LIGHT
        if Settings.get_default().props.dark_theme:
            color = Adw.ColorScheme.PREFER_DARK
        Adw.StyleManager.get_default().set_property(
            'color-scheme',
            color
        )

    def on_about(self, *_):
        builder = Gtk.Builder.new_from_resource(
            '/com/gitlab/guillermop/MasterKey/ui/about.ui')
        about = builder.get_object('about_dialog')
        about.set_transient_for(self.props.active_window)
        about.set_logo_icon_name(self.get_application_id())
        about.set_version(self.version)
        about.present()

    def on_reset(self, *args):
        window = self.props.active_window
        builder = Gtk.Builder.new_from_resource(
            '/com/gitlab/guillermop/MasterKey/ui/delete.ui')
        delete = builder.get_object('delete_dialog')
        delete.set_transient_for(self.props.active_window)

        def on_response(d, response):
            if response == Gtk.ResponseType.OK:
                Database.get_default().reset()
                window.list_box.remove_all()
                self.is_locked = True
            d.destroy()

        delete.connect("response", on_response)
        delete.present()

    def on_quit(self, *args):
        self.props.active_window.close()

