# label.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, GObject


class Label(Gtk.Label):
    __gtype_name__ = 'Label'

    visibility = GObject.Property(type=bool, default=False)
    real_text = ''

    def __init__(self):
        Gtk.Label.__init__(self)
        self.connect('notify::visibility', self.update_text)

    def update_text(self, *args):
        if self.props.visibility:
            text = self.real_text
        else:
            l = len(self.real_text)
            mask = 1
            if l > 15:
                mask = 3
            elif l > 7:
                mask = 2
            text = self.real_text[0:mask] + '•' * (l - mask * 2) + self.real_text[-mask:]
        Gtk.Label.set_text(self, text)

    def get_text(self):
        return self.real_text

    def set_text(self, text):
        self.real_text = text
        if text:
            self.update_text()
        
