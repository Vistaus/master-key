# preferences.py
#
# Copyright 2021 Guillermo Peña
#
# This file is part of Master Key.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gtk, Gio, Adw
from master_key import Settings


@Gtk.Template(resource_path='/com/gitlab/guillermop/MasterKey/ui/preferences.ui')
class Preferences(Adw.PreferencesWindow):
    __gtype_name__ = 'Preferences'

    seconds_spin_button = Gtk.Template.Child()
    minutes_spin_button = Gtk.Template.Child()
    dark_switch = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        settings = Settings.get_default()
        settings.bind("dark-theme", self.dark_switch, "active", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("clipboard-timeout", self.seconds_spin_button, "value", Gio.SettingsBindFlags.DEFAULT)
        settings.bind("lock-timeout", self.minutes_spin_button, "value", Gio.SettingsBindFlags.DEFAULT)

