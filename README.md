<a href='https://flathub.org/apps/details/com.gitlab.guillermop.MasterKey'><img width='240' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>

# Master Key

Master Key is a password manager application built with Python 3 and GTK that generates and manages passwords without the need to store them

<div align="center">
![screenshot](data/screenshots/screenshot1.png)
</div>

## Features

- Passwords are generated using a combination of a master key, a site and a login
- Passwords are never stored
- Passwords can be recreated everywhere
- The accounts information is encrypted
- Follows the GNOME Human Interface Guidelines

## Building

You can build Master Key with Gnome Builder, just clone the project and hit the run button.

### Requirements
- `meson >= 0.50`
- `gtk4 >= 4.0.0`
- `python3`
- `libadwaita1 >=1.0.0`
- `ninja`
- `pygobject >= 3.40.0`
- `libpwquality`
- `sqlcipher`


### Building with Meson

```bash
git clone https://gitlab.com/guillermop/master-key.git
cd master-key
meson . _build --prefix=/usr
ninja -C _build
sudo ninja -C _build install
```

## Credits

- Master Key is inspired by [privacy-friendly-passwordgenerator](https://github.com/SecUSo/privacy-friendly-passwordgenerator)
- Code is based on the legacy version of [Authenticator](https://gitlab.gnome.org/World/Authenticator)
